from django.urls import path
from . import views

urlpatterns=[
    path('add',views.add_review,name='add_review'),
    path('',views.all_review,name='all_review'),

    path('<str:username>',views.review_ops,name='review_ops'),

]
