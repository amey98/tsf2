
from rest_framework import serializers

from review.models import Review

class reviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ['UserName','Movie','Review']
