from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
import json
from .serializers import reviewSerializer

from review.models import Review




@api_view(["POST"])
def add_review(request):

        Movie=request.POST.get('Movie')
        UserName=request.POST.get('UserName')
        Reviews=request.POST.get('Review')


        t=Review(Movie=Movie,UserName=UserName,Review=Reviews)
        t.save()
        return JsonResponse("Added Review details",safe=False)

@api_view(["GET"])
def all_review(request):
    if request.method == 'GET':

        t=Review.objects.all()
        serializer = reviewSerializer(t, many=True)
        return Response(serializer.data)


@api_view(['GET', 'POST','DELETE'])
def review_ops(request,username):
    if request.method == 'DELETE':
        try:
            t=Review.objects.get(UserName=username)
        except:
            return JsonResponse("Error",safe=False)
        t.delete()
        return JsonResponse("Deleted review successfully",safe=False)

    elif request.method == 'GET':

        try:
            t=Review.objects.filter(UserName=username)
            if len(t)==0:
                return JsonResponse("No such review in database",safe=False)
        except:
            return JsonResponse("Error",safe=False)

        serializer = reviewSerializer(t, many=True)
        return Response([serializer.data])

    elif request.method == 'POST':
        try:
            t = Review.objects.get(UserName=username)

        except Review.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        t.Review=request.POST.get('Review')

        t.save()
        return JsonResponse("successfully updated record",safe=False)
