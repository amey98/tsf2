from django.db import models

# Create your models here.
class Review(models.Model):
    Movie=models.CharField(max_length=40)
    UserName=models.CharField(max_length=40,primary_key=True)
    Review=models.CharField(max_length=40)
    
