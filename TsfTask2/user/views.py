from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
import json
from user.models import User

from user.serializers import userSerializer
# Create your views here.

@api_view(["POST"])
def add_user(request):
    try:
        Name=request.POST.get('Name')
        UserName=request.POST.get('UserName')
        password=request.POST.get('password')
        email=request.POST.get('email')
        contact=request.POST.get('contact')

        s=User(Name=Name,UserName=UserName,password=password,email=email,contact=contact)
        s.save()
        return JsonResponse("Added USER "+s.UserName+" details",safe=False)
    except:
        return JsonResponse("Error",safe=False)


@api_view(["GET"])
def all_users(request):
    if request.method == 'GET':
        #print('hiiiiiiii')
        s=User.objects.all()
        serializer = userSerializer(s, many=True)
        return Response(serializer.data)


@api_view(['GET', 'POST','DELETE'])
def user_ops(request,username):
    if request.method == 'DELETE':
        try:
            t=User.objects.get(UserName=username)
        except:
            return JsonResponse("Error",safe=False)
        t.delete()
        return JsonResponse("Deleted user successfully",safe=False)

    elif request.method == 'GET':

        try:
            s=User.objects.filter(UserName=username)
            if len(s)==0:
                return JsonResponse("No such user in database",safe=False)
        except:

            return JsonResponse("Error",safe=False)

        serializer = userSerializer(s, many=True)



        return Response([serializer.data])

    elif request.method == 'POST':
        try:
            s = User.objects.get(UserName=username)
        
        except :
            return JsonResponse("No such user in database",safe=False)
        s.Name=request.POST.get('Name')
        s.email=request.POST.get('email')
        s.contact=request.POST.get('contact')
        s.save()
        return JsonResponse("successfully updated user record",safe=False)
