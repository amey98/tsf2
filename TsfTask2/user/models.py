from django.db import models

# Create your models here.
class User(models.Model):
    Name=models.CharField(max_length=40)
    UserName=models.CharField(max_length=40,primary_key=True)
    password=models.CharField(max_length=400)
    email=models.CharField(max_length=40)
    contact=models.CharField(max_length=10)
