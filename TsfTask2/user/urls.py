from django.urls import path
from . import views

urlpatterns=[
    path('add',views.add_user,name='add_user'),

    path('',views.all_users,name='all_users'),
    path('<str:username>',views.user_ops,name='user_ops'),

]
