from django.urls import path
from . import views

urlpatterns=[
    path('add',views.add_movie,name='add_movie'),
    path('',views.all_movies,name='all_movies'),


    path('<str:cid>',views.movie_ops,name='movie'),

]
