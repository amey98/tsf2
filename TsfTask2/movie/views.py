from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
import json
from .serializers import movieSerializer
from review.models import Review
from user.models import User
from movie.models import Movie
# Create your views here.
@api_view(["POST"])
def add_movie(request):
        Name=request.POST.get('Name')
        Type=request.POST.get('Type')
        Year=request.POST.get('Year')
        Language=request.POST.get('Language')
        m=Movie.objects.filter(Name=Name)
        if len(m)>0:
            return JsonResponse("movie "+ m[0].Name + " already exists",safe=False)
        m=Movie(Name=Name,Type=Type,Year=Year,Language=Language)
        m.save()
        return JsonResponse("Added Movie "+ m.Name + " details",safe=False)


@api_view(["GET"])
def all_movies(request):
    if request.method == 'GET':
        m=Movie.objects.all()
        serializer = movieSerializer(m, many=True)
        return Response(serializer.data)


@api_view(['GET', 'POST','DELETE'])
def movie_ops(request,cid):
    if request.method == 'DELETE':
        try:
            m=Movie.objects.get(Name=cid)
        except:
            return JsonResponse("No such Movie",safe=False)
        m.delete()
        return JsonResponse("Deleted Movie " +m.Name +" successfully",safe=False)

    elif request.method == 'GET':
        try:
            m=Movie.objects.filter(Name=cid)
            if len(m)==0:
                return JsonResponse("No such Movie in database",safe=False)
        except:
            return JsonResponse("Error",safe=False)

        serializer = movieSerializer(c, many=True)
        return Response([serializer.data])

    elif request.method == 'POST':
        try:
            m=Movie.objects.get(Name=cid)
            
        except Movie.DoesNotExist:
            return JsonResponse("No such movie in database",safe=False)
        m.Language=request.POST.get('Language')

        m.save()
        return JsonResponse("successfully updated movie "+ m.Name +".",safe=False)
