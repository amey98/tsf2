from django.db import models

# Create your models here.
class Movie(models.Model):
    Name=models.CharField(max_length=40)
    Type=models.CharField(max_length=40,primary_key=True)
    Year=models.CharField(max_length=400)
    Language=models.CharField(max_length=40)
